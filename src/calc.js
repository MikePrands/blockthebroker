// Import trading configurations
import RiskManagement from './trading-config';
import getTickers from './kraken-tikers';
import {getFromRedis} from './redis';

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// SETTINGS
if (!String.prototype.splice) {
  /**
   * {JSDoc}
   *
   * The splice() method changes the content of a string by removing a range of
   * characters and/or adding new characters.
   *
   * @this {String}
   * @param {number} start Index at which to start changing the string.
   * @param {number} delCount An integer indicating the number of old chars to remove.
   * @param {string} newSubStr The String that is spliced in.
   * @return {string} A new string with the spliced substring.
   */
  String.prototype.splice = function(start, delCount, newSubStr) {
      return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
  };
}


// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// CALCULUS
// Calculate take profit of current
const calcCurrentPrice = async (cryptoPair) => {
  let currentPairPrice = await getTickers(cryptoPair);
  // let priceFromPercentage = currentPairPrice[0][2] + (currentPairPrice[0][2] / 100) * RiskManagement.takeProfitPercentage; // X% more than actual price

  console.log('calcCurrentPrice() Actual Pair Price:', currentPairPrice[0][2]);
  // console.log('calcCurrentPrice() Taking profit at:', priceFromPercentage.toFixed(5));

  // Adjust fixed decimals
  // if(cryptoPair === 'BCHUSD') {
  //   priceFromPercentage = priceFromPercentage.toFixed(1);
  // } else if(cryptoPair === 'XETHZUSD') {
  //   priceFromPercentage = priceFromPercentage.toFixed(2);
  // } else {
  //   priceFromPercentage = priceFromPercentage.toFixed(5);
  // }

  // Return price of take profit
  return currentPairPrice[0][2];
}

const calculateStopLoss = async (cryptoPair) => {
  let currentPairPrice = await getTickers(cryptoPair);
  let priceFromPercentage = currentPairPrice[0][2] - (currentPairPrice[0][2] / 100) * RiskManagement.takeProfitPercentage; // X% less than actual price

  console.log('calculateStopLoss() Actual Pair Price:', currentPairPrice[0][2]);
  console.log('calculateStopLoss() Stop Loss at:', priceFromPercentage.toFixed(5));

  // Adjust fixed decimals
  if(cryptoPair === 'BCHUSD') {
    priceFromPercentage = priceFromPercentage.toFixed(1);
  } else if(cryptoPair === 'XETHZUSD') {
    priceFromPercentage = priceFromPercentage.toFixed(2);
  } else {
    priceFromPercentage = priceFromPercentage.toFixed(5);
  }

  // Return price of take profit
  return priceFromPercentage;
}

const calculateLotVolume = async (cryptoPair) => { // How many crypto to buy calculating the risk balance
  let walletBalance = RiskManagement.isTesting ? RiskManagement.tradeCapitalRisk : await getWalletStatus(); // in USD
  let balanceAmount = RiskManagement.isTesting ? walletBalance : walletBalance.currentBalance;
  let currentPairPrice = await getTickers(cryptoPair); // Get current crypto pair price
  let calculateLotVolume = balanceAmount / currentPairPrice[0][2]; // Obtain how many crypto to buy

  console.log('calculateLotVolume() Capital to use (USD):', balanceAmount);
  console.log('calculateLotVolume() Available Lot volume:', calculateLotVolume.toFixed(5));

  // Return calculated volume
  return calculateLotVolume.toFixed(5);
}


// Calc estimated profit of current
const calcEstimatedProfit = async (orderTaxId)=> {
  const orderPair = await getFromRedis(orderTaxId + '__pair');
  const originalPairPrice = await getFromRedis(orderTaxId + '__pairPrice');
  const capitalUsed = await getFromRedis(orderTaxId + '__capitalUsed');
  const currentPrice = await calcCurrentPrice(orderPair);
  const newPrice = (currentPrice * capitalUsed) / originalPairPrice;
  const estimatedProfit = newPrice - capitalUsed;

  // Debug
  // console.log('calcEstimatedProfit() orderPair:', orderPair);
  // console.log('calcEstimatedProfit() originalPairPrice:', originalPairPrice);
  // console.log('calcEstimatedProfit() capitalUsed:', capitalUsed);
  // console.log('calcEstimatedProfit() currentPrice:', currentPrice);
  // console.log('calcEstimatedProfit() estimatedProfit:', estimatedProfit);

  return estimatedProfit.toFixed(2);
}

// const CalculatingRiskPreview = async function() {
//   await calculateTakeProfit('ETHUSD');
//   await calculateLotVolume('ETHUSD');
// }

// CalculatingRiskPreview()

module.exports = {
  calculateLotVolume,
  calculateStopLoss,
  calcCurrentPrice,
  calcEstimatedProfit,
}