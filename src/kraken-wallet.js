import kraken from './kraken-config';

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// Get Wallet Info
// return an object of current wallet info

const getWalletStatus = async () => {
  const wallet = {};

  await kraken.call('TradeBalance', { asset: 'USD' })
  .then(
    _balance => {
      wallet.currentBalance = _balance.eb;
      // balance.balanceDiff = balance.currentBalance - balance.oldSessionBalance;
      // console.log('Current Balance:', balance.currentBalance);
      // console.log('Old Session Balance:', balance.oldSessionBalance);
      // console.log('Balance Diff:', balance.balanceDiff);
      // session.balance = balance.currentBalance;
      // session.saveSession('old_balance', balance.currentBalance);
      // console.log('New Session Balance:', session.balance);
    }
  )
  .catch(
    err => {
      wallet.error = err;
      console.error('ERRORE WALLET GET STATUS: Non é stato possibile recuperare il tuo conto.');
      console.error(err);
    }
  );

  return wallet;
}

module.exports = getWalletStatus;