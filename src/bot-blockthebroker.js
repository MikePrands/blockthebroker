// Importing dotEnv && Initializations environmental
import dotenv from 'dotenv';
dotenv.config();
// TELEGRAF IMPORTS
import Telegraf from 'telegraf';
import Markup from 'telegraf/markup';
// Kraken orders imports
import {newOrder, getOpenOrders, closeOpenOrders, getOrdersInfo, newTakeProfitOrder} from './kraken-orders';
import getTickers from './kraken-tikers';
import getWalletStatus from './kraken-wallet';
import {calcEstimatedProfit} from './calc';
import {saveToRedis, getFromRedis} from './redis';
import {newWalletMessage, newTickerMessage} from './messages';
// Import trading configurations
// import RiskManagement from './trading-config';
import _ from 'lodash';

// Instance Telegraf
const bot = new Telegraf(process.env.BOT_TOKEN);

settingsInit();

const BlockTheBroker = () => {

  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // ACTIONS

  // Open position
  bot.action(/(^OP)(_)(.+)/, async (ctx) => {
    let setNewOrder = await newOrder(ctx.match[3], 'buy');

    if(setNewOrder.success && !setNewOrder.err) {
      ctx.answerCbQuery(`✅ Opening position...`);
      // let orderTypeOpened = setNewOrder.info;
      let newOrderCreatedInfo = setNewOrder.order;

      return ctx.replyWithHTML(`✅ Ordine <strong>${ctx.match[3]}</strong> aggiunto (${newOrderCreatedInfo.taxId})!\nToken acquistati: ${newOrderCreatedInfo.lotVolume} \nSpesi $${newOrderCreatedInfo.capitalUsed} \nValore pair acquisto: $${newOrderCreatedInfo.pairPrice} \nStop loss: $${newOrderCreatedInfo.stopLoss}`);
    } else {
      return ctx.replyWithHTML(`Errore: ${setNewOrder.err}`)
    }
  });

  // Close position
  bot.action(/(^CP)(_)(.+)/, async (ctx) => {
    console.log('Closing position:', ctx.match);
    const closeOpenOrder = await closeOpenOrders(ctx.match[3]);

    if (!closeOpenOrder.err) {
      let messageCloseOpenOrder = closeOpenOrder.pending ? `Ordine <strong>${ctx.match[3]}</strong> in chiusura. Ricontrolla a breve l'avvenuta chiusura.` : `Ordine <strong>${ctx.match[3]}</strong> chiuso.`;
      ctx.answerCbQuery(`❌ Closing position...`);
      return ctx.replyWithHTML(messageCloseOpenOrder);
    } else {
      return ctx.replyWithHTML(`!!! Errore chiusa ordine <strong>${ctx.match[3]}</strong>!`);
    }
  });

  // Take profit from position
  bot.action(/(^TP)(_)(.+)/, async (ctx) => {
    // console.log('Taking profit of:', ctx.match);
    const takeProfit = await newTakeProfitOrder(ctx.match[3]);

    if (!takeProfit.err) {
      ctx.answerCbQuery(`Taking profit...`);
      let messageCloseOpenOrder = `Ottenuto profit da ordine <strong>${ctx.match[3]}</strong>}`;
      // console.log('Ordine take profit', takeProfit.order);
      return ctx.replyWithHTML(messageCloseOpenOrder);
    } else {
      return ctx.replyWithHTML(`!!! Errore durante Taking Profit <strong>${ctx.match[3]}</strong>! ${takeProfit.err}`);
    }
  });

  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // COMMANDS
  bot.command('start', ({ reply }) => {
    return reply('Seleziona una voce di menu', Markup
      .keyboard([
        [
          Markup.callbackButton('📊 Ticker', 'ticker'),
          Markup.callbackButton('💰 Wallet', 'wallet'),
        ],
        [
          Markup.callbackButton('🐃 Open Orders', 'positions'),
          Markup.callbackButton('⚙️ Settings', 'settings'),
        ]
      ])
      .resize()
      .extra()
    )
  });


  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // HEARS and REACTIONS
  // GET BALANCE WALLET
  // REPLY WITH WALLET BALANCE
  bot.hears('💰 Wallet', async (ctx) => {
    const walletBalance = await getWalletStatus();

    if(!walletBalance.error) {
      let oldBalance = await getFromRedis('wallet_balance');
      let balanceMessage = newWalletMessage(walletBalance.currentBalance, oldBalance);
      ctx.replyWithHTML(balanceMessage);
      await saveToRedis('wallet_balance', walletBalance.currentBalance);
    } else {
      ctx.replyWithHTML(`‼️ Errore nel recupero del wallet.`);
    }
  });

  // GET TICKERS
  // REPLY WITH TICKERS
  bot.hears('📊 Ticker', async (ctx) => {
    const tickers = await getTickers();
    // console.log('⚠️ TICKERS:', tickers);

    if(!tickers.error) {
      // Loop array
      for (let i in tickers) {

        //console.log('⚠️ SINGLE TICKER:', tickers[i]);
        let oldVPairalue = await getFromRedis(tickers[i][0]);
        let pairTickerMessage = newTickerMessage(tickers[i][1], oldVPairalue, tickers[i][2]);

        ctx.replyWithHTML(`${ pairTickerMessage }`,
        Markup.
          inlineKeyboard([
            Markup.callbackButton(`✅ Op. Pos. (0)`, 'OP_' + tickers[i][0])
          ]).extra());

        await saveToRedis(tickers[i][0], tickers[i][2]);
        // console.log('⚠️ testRedisSessionSave:', testRedisSessionSave);
      }
    } else {
      ctx.replyWithHTML(`‼️ Errore nel recupero dei tickers.`);
    }
  });

  // GET OPEN ORDERS
  // REPLY WITH WALLET BALANCE
  bot.hears('🐃 Open Orders', async (ctx) => {
    const orders = await getOpenOrders();
    // console.log('hears(\'🐃 Open Orders\') orders:', orders);
    const taxIds = orders.taxIds;
    const arrOrders = _.toPairs(orders.list);
    // console.log('hears(\'🐃 Open Orders\') arrOrders:', arrOrders);

    if(orders.list.length >= 1) {

      for(let i in arrOrders) {
        let order = arrOrders[i][1];
        // console.log('hears(\'🐃 Open Orders\') ORDER:', order);
        // console.log('hears(\'🐃 Open Orders\') TAXID:', taxIds[i]); // Pass it to getOrdersInfo()
        //let orderInfo = await getOrdersInfo(orders.taxIds[i]);
        console.log('🐃 Order TAXID: ', orders.taxIds[i]);
        //const openOrderInfo = await getOpenOrderInfo(orders.taxIds[i]);
        const estimatedProfit = await calcEstimatedProfit(order.refid);
        const orderInfo = await getOrdersInfo(order.refid); // REFID Necessary
        const estimatedReturn = parseFloat(orderInfo.capitalUsed) + parseFloat(estimatedProfit);
        console.log('orderInfo.capitalUsed', orderInfo.capitalUsed);
        console.log('estimatedProfit', estimatedProfit);
        const orderMessage = `Ordine <strong>${orderInfo.pair}</strong> (${order.refid})\nToken acquistati: ${orderInfo.lotVolume} \nSpesa: $${orderInfo.capitalUsed} \n---------\nPrezzo acquisto: $${orderInfo.originalPairPrice}\nPrezzo corrente: $${orderInfo.currentPrice}\n---------\nStop loss: $${orderInfo.stopLoss}\n---------\nRitorno da vendita: $${estimatedReturn.toFixed(2)}\nTake profit: $${estimatedProfit}`;

        // let orderMessage = `<strong># ${order.refid} ${order.descr.pair}</strong> \nOrder: ${orderInfo.order} \nClose: ${orderInfo.close}`;
        ctx.replyWithHTML(orderMessage,
          Markup.
            inlineKeyboard([
              Markup.callbackButton(`💵 Take Profit`, `TP_${orders.taxIds[i]}`),
              Markup.callbackButton(`❌ Close Position`, `CP_${orders.taxIds[i]}`),
            ]).extra());
      }
    } else {
      let orderMessage = `NON HAI ORDINI APERTI.`;
      ctx.replyWithHTML(orderMessage);
    }
  });

  // GET SETTINGS
  // REPLY WITH SETTINGS STATUS
  // bot.hears('⚙️ Settings', (ctx) => {
  //   let settingMessage = `⚙️ Settings: \nModalitá test: <strong>${RiskManagement.isTesting}</strong> \nTipo di ordine: <strong>${RiskManagement.orderType}</strong> \nCapitale per ogni trade: USD <strong>${RiskManagement.tradeCapitalRisk}</strong> \nTake profit a: <strong>+${RiskManagement.takeProfitPercentage}%</strong>`;
  //   ctx.replyWithHTML(settingMessage);
  // });


  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------
  // BOT STARTS
  bot.launch();

}

module.exports = BlockTheBroker;