// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// COMMMUNICATING CHAT MESSAGES

// Ticker Message
const newTickerMessage = (pairName, pairOldPrice, pairCurrentPrice) => {
  let priceVariation = (pairCurrentPrice - pairOldPrice).toFixed(5);
  let priceUpOrDown = pairCurrentPrice > pairOldPrice ? '🔼' : '🔽';

  let message = `<strong>${pairName}</strong> \n💵  Prezzo: $<strong>${pairCurrentPrice}</strong> USD \n${priceUpOrDown}  Var: ${priceVariation} USD`;

  return message;
}

// Ticker Message
const newWalletMessage = (walletNewBalance, walletOldBalance) => {
  let priceVariation = walletNewBalance - walletOldBalance;
  let priceUpOrDown = walletNewBalance > walletOldBalance ? '🔼' : '🔽';

  let message = `<strong>Hai a disposizione:</strong> \n💰  <strong>$${walletNewBalance.toFixed(5)} USD</strong> \n${priceUpOrDown}  Var: ${(priceVariation).toFixed(5)} USD`;

  return message;
}

//

module.exports = {
  newTickerMessage: newTickerMessage,
  newWalletMessage: newWalletMessage
}