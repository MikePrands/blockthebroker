// Importing dotEnv && Initializations environmental
import dotenv from 'dotenv';
dotenv.config();
// KRAKEN IMPORTS
import KrakenExchange from 'node-kraken-api';

// Instance Kraken
const kraken = KrakenExchange({
  key: process.env.KRAKEN_API,
  secret: process.env.KRAKEN_PRIVATE_KEY
});

module.exports = kraken;