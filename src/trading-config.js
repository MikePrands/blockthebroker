import {saveToRedis, getFromRedis} from 'redis';

// TRADING RISK CONFIGURATIONS
// DO NOT directly access this object!!!
// const settingsRiskManagement = {
//   isTesting: true,            // TRUE for testing purpose only
//   orderType: 'market',        // Taking profit
//   tradeCapitalRisk: 10,       // In percentage ### During test is set in USD ###
//   takeProfitPercentage: 5,    // In percentage. Always %
//   stopLossPercentage: 5,      // In percentage. Always %
//   fixedLotVolume: 0.3,        // Fixed lot volumes
// }

class RiskManagement {
  constructor (settingsRiskManagement) {
    this.isTesting = settingsRiskManagement.isTesting;
    this.orderType = settingsRiskManagement.orderType;
    this.tradeCapitalLimit = settingsRiskManagement.tradeCapitalRisk;
    this.takeProfitPercentage = settingsRiskManagement.takeProfitPercentage;
    this.stopLossPercentage = settingsRiskManagement.stopLossPercentage;
    this.fixedLotVolume = settingsRiskManagement.fixedLotVolume;
  }

  // updateSettings = (setting, newValue) => {
  //   let feedbackSuccess = {};

  //   switch (setting) {
  //     case 'isTesting':
  //     break;

  //     case 'orderType':
  //     break;

  //     // And so on...

  //     default:
  //       feedbackSuccess.err = 'no update configuration found.';
  //       console.log(feedbackSuccess.err);
  //   }

  //   return feedbackSuccess;
  // }


  // Return the list of settings Names
  getSettingsListNames = () => {
    const settingsArr = [];
    return settingsArr;
  }

  // Return the list of settings values
  getSettingsListValues = () => {
    const settingsArr = [];
    return settingsArr;
  }

  // Check if settings exist on reddit
  areSettingsOnReddit = () => {
    getFromRedis()
  }

  init = () => {
    // Check if riskManagement exists on redis
    // IF NOT EXISTS
      // USE default
    // IF EXISTS
      // USE redis
  }
}



module.exports = {
  RiskManagement
};