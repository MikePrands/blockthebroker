// Import Kraken config
import kraken from './kraken-config';
import _ from 'lodash';
// Import trading configurations
import RiskManagement from './trading-config';
// Calc imports
import {calculateLotVolume, calculateStopLoss, calcCurrentPrice} from './calc';
import {saveToRedis, getFromRedis} from './redis';
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// KRAKEN ORDERS


// SET NEW ORDER
// INPUT pair and type order
const newOrder = async (pair, type, orderVolume) => {
  let orderOpeningSuccess = {
    sucess: false,
    order: {}
  };
  let orderDetails = {};
  const lotVolume = orderVolume || await calculateLotVolume(pair);
  const currentPrice = await calcCurrentPrice(pair);
  const priceStopLoss = await calculateStopLoss(pair);

  if(RiskManagement.orderType === 'market' && type === 'buy') {
    // Define order details
    orderDetails = {
      pair: pair,
      ordertype: RiskManagement.orderType, // Should be market
      type: type,
      volume: lotVolume,
      close: {
        ordertype: 'stop-loss',
        price: priceStopLoss // calculate STOP LOSS, now is 5%
      }
    }
  } else if(RiskManagement.orderType === 'market' && type === 'sell'){
    orderDetails = {
      pair: pair,
      ordertype: RiskManagement.orderType, // Should be market
      type: type,
      volume: lotVolume
    }
  }

  await kraken.call('AddOrder', orderDetails)
    .then((data) => {
      orderOpeningSuccess.success = true;
      console.log('', data);

      // SAVE txId infos
      saveToRedis(data.txid[0] + '__capitalUsed', RiskManagement.tradeCapitalRisk);
      saveToRedis(data.txid[0] + '__lotVolume', lotVolume);
      saveToRedis(data.txid[0] + '__pairPrice', currentPrice);
      saveToRedis(data.txid[0] + '__stopLoss', priceStopLoss);
      saveToRedis(data.txid[0] + '__pair', pair);

      // Add to return the info
      orderOpeningSuccess.order.taxId = data.txid[0];
      orderOpeningSuccess.order.capitalUsed = RiskManagement.tradeCapitalRisk;
      orderOpeningSuccess.order.lotVolume = lotVolume;
      orderOpeningSuccess.order.pairPrice = currentPrice;
      orderOpeningSuccess.order.stopLoss = priceStopLoss;
      orderOpeningSuccess.order.pair = pair;

      saveToRedis(data.txid[0], JSON.stringify(data.descr));
      // orderOpeningSuccess.info = data.descr;
      console.log(' 🔆 🔆 🔆 KRAKEN ORDER DESCR', data.descr);
      console.log(' 🔆 🔆 🔆 KRAKEN ORDER TXID', data.txid[0]);
    })
    .catch((err) => {
      orderOpeningSuccess.success = false;
      orderOpeningSuccess.err = err;
      console.log(' ❌ ❌ ❌ KRAKEN ORDER  ❌ ❌ ❌', err);
    });

  return orderOpeningSuccess;
}

const getOpenOrders = async () => {
  let orders = {};
  orders.taxIds = [];
  orders.list = [];

  // GET ALL OPEN ORDER ID
  await kraken.call('OpenOrders', {trades: true})
    .then((data) => {
      const ordersArr = _.toPairs(data.open);
      console.log('getOpenOrders() ordersArr:', data);

      for (let i in ordersArr) {
        let currentOrder = {};
        currentOrder = ordersArr[i][1];
        // console.log('CURRENT ORDER REFID:', currentOrder.refid);
        // console.log('CURRENT ORDER DESCR', currentOrder.descr);
        orders.taxIds.push(ordersArr[i][0]);
        orders.list.push(currentOrder);
      }
    })
    .catch((err) => {
      orders.err = err;
      console.log(' ❌ ❌ ❌ KRAKEN OPENING ORDER  ❌ ❌ ❌', err);
    });

  return orders;
}

// Get EXECUTED order info. NOT FOR ACTIVE ORDERs
// Input Order REFID
const getOrdersInfo = async (orderRefId) => {
  const order = {}

  order.pair = await getFromRedis(orderRefId + '__pair');
  order.originalPairPrice = await getFromRedis(orderRefId + '__pairPrice');
  order.capitalUsed = await getFromRedis(orderRefId + '__capitalUsed');
  order.lotVolume = await getFromRedis(orderRefId + '__lotVolume');
  order.stopLoss = await getFromRedis(orderRefId + '__stopLoss');

  const currentPrice = await calcCurrentPrice(order.pair);
  order.currentPrice = currentPrice;
  const newPrice = (currentPrice * order.capitalUsed) / order.originalPairPrice;
  order.estimatedProfit = newPrice - order.capitalUsed;

  return order;
}


// Get info for a CURRENT OPEN order
// const getOpenOrderInfo = async (orderTaxId)=> {
//   const order = {};
//   order.taxIds = [];
//   order.list = [];

//   // GET ALL OPEN ORDER ID
//   await kraken.call('QueryOrders',
//     {
//       txid: orderTaxId, trades: true
//     })
//       .then((data) => {
//         console.log('getOpenOrderInfo() data from API CALL:', data);
//         console.log('getOpenOrderInfo() _.toPairs(data) from API CALL:', _.toPairs(data));
//         const orderArr = _.toPairs(data);

//         for (let i in orderArr) {
//           let currentOrder = {};
//           currentOrder = orderArr[i][1];
//           console.log('getOpenOrderInfo() REFID:', currentOrder.refid);

//           order.taxIds.push(orderArr[i][0]);
//           order.list.push(currentOrder);
//         }
//       })
//       .catch((err) => {
//         order.err = err;
//         console.log(' ❌ ❌ ❌ Errore API CALL getOpenOrderInfo():', err);
//       });

//   return order;
// }
const getOpenOrderRefId = async (orderTaxId)=> {
  const order = {};

  // GET ALL OPEN ORDER ID
  await kraken.call('QueryOrders',
    {
      txid: orderTaxId, trades: true
    })
      .then((data) => {
        const orderArr = _.toPairs(data);
        // console.log('getOpenOrderRefId() data:', data);

        for (let i in orderArr) {
          let currentOrder = {};
          currentOrder = orderArr[i][1];
          order.refid = currentOrder.refid;
          // console.log('getOpenOrderInfo() REFID:', currentOrder.refid);
        }
      })
      .catch((err) => {
        order.err = err;
        console.log(' ❌ ❌ ❌ Errore API CALL getOpenOrderInfo():', err);
      });

  return order;
}

const closeOpenOrders = async (taxId) => {
  const closeOrderSuccess = {};

  // GET ALL OPEN ORDER ID
  await kraken.call('CancelOrder', {txid: taxId})
    .then((data) => {
      if(data.count === 1 && !data.pending) {
        closeOrderSuccess.success = true;
        closeOrderSuccess.pending = false;
      } else if(data.pending) {
        closeOrderSuccess.success = true;
        closeOrderSuccess.pending = true;
      }
    })
    .catch((err) => {
      closeOrderSuccess.err = err;
      console.log(' ❌ ❌ ❌ KRAKEN CLOSING ORDER  ❌ ❌ ❌', err);
    });

  return closeOrderSuccess;
}

const newTakeProfitOrder = async (orderTaxId) => {
  const takeProfitSuccess = {};
  // Get OpenOrderInfo by taxID
  const getOrderRefId = await getOpenOrderRefId(orderTaxId);

  if(!getOrderRefId.err) {
    const orderInfo = await getOrdersInfo(getOrderRefId.refid);
    // console.log('newTakeProfitOrder() getOrderRefId:', getOrderRefId);
    // console.log('newTakeProfitOrder() orderInfo:', orderInfo);

    // IF getOrdersInfo is OK and NO ERR
    if(!orderInfo.err) {
      // Go and Close order
      const closeOrder = await closeOpenOrders(orderTaxId);

      // IF closeOpenOrders is OK and NO ERR
      if(!closeOrder.err) {
        let volume = parseFloat(orderInfo.lotVolume).toFixed(2);

        // Go and Create a new SELL order
        let setNewOrder = await newOrder(orderInfo.pair, 'sell', volume);

        // IF newOrder is OK and NO ERR
        if(!setNewOrder.err) {
          takeProfitSuccess.success = true;
          takeProfitSuccess.order = orderInfo;
        } else {
          takeProfitSuccess.err = 'Errore durante esecuzione nuovo ordine SELL.';
        }
      }
    } else {
      takeProfitSuccess.err = closeOrder.err;
    }
  } else {
    takeProfitSuccess.err = 'Non è stato possibile recuperare la refid dell ordine.';
  }

  return takeProfitSuccess;
}

module.exports = {
  newOrder,
  getOpenOrders,
  getOrdersInfo,
  getOpenOrderRefId,
  closeOpenOrders,
  newTakeProfitOrder
}