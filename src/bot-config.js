// Importing dotEnv && Initializations environmental
import dotenv from 'dotenv';
dotenv.config();
// TELEGRAF IMPORTS
import Telegraf from 'telegraf';
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// Instance Telegraf
const bot = new Telegraf(process.env.BOT_TOKEN);

module.exports = bot;
