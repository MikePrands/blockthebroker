import kraken from './kraken-config';
import _ from 'lodash';
// Import trading configurations
import CryptoPairs from './crypto-pair-config';
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// Define Ticker Info Call
// return an array of objects
const getTickers = async (pair) => {

  const tickers = []
  let currencyPairs = pair || CryptoPairs.join();

  // Call kraken API ticker information
  await kraken.call(
    'Ticker',
    { pair: currencyPairs } // Get only current pair of loop array
  )
  .then(
    (data) => {

      const tickering = _.toPairs(data);

      for (let i in tickering) {
        let ticker = [];

        ticker[0] = tickering[i][0];
        ticker[1] = ticker[0];
        ticker[2] = tickering[i][1].a[0];

        if(ticker[1] === 'XXBTZUSD') {
          ticker[1] = 'XBT / USD';
        } else if(ticker[1] === 'XETHZUSD') {
          ticker[1] = 'ETH / USD';
        } else if(ticker[1] === 'XLTCZUSD') {
          ticker[1] = 'LTC / USD';
        } else {
          ticker[1] = ticker[1].splice(3, 0, " / ");
        }

        tickers.push(ticker);
      }
    }
  )
  .catch(
    (err) => {
      tickers.error = err;
      console.error('️ℹ️ ❗️ Error:', err);
    }
  );
  // console.log('ℹ️ TICKERS LOG:', tickers);
  return tickers;
};

module.exports = getTickers;