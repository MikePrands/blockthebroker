import bot from './bot-config';
import Markup from 'telegraf/markup';
import getWalletStatus from './kraken-wallet';
import {saveToRedis, getFromRedis} from './redis';
import getTickers from './kraken-tikers';
import {newTickerMessage, newWalletMessage} from './messages';
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

// SET INTERVAL FOR AUTO TICKER
// Instance of CHATID
const ChatID = 9636607; // My personal Chat ID


// Time frame di trading, [in minutes]
// Trading Ours from, to [0-24 | all]
// Days per week [0-7 | all]
const tickerTimeFrame = 60;


const autoNotifications = async () => {

  // Set interval in order to trigger notifications
  setInterval(async () => {
    const message = ['`--- ⏱ AGGIORNAMENTO ORARIO ---`']
    const tickers = await getTickers();
    const walletBalance = await getWalletStatus();

    if(!tickers.error) {
      // Loop array
      for (let i in tickers) {
        let oldVPairalue = await getFromRedis(tickers[i][0]);
        let pairTickerMessage = newTickerMessage(tickers[i][1], oldVPairalue, tickers[i][2]);

        message.push(pairTickerMessage)

        await saveToRedis(tickers[i][0], tickers[i][2]);
      }
    } else {
      bot.telegram.sendMessage(ChatID, `‼️ Errore nel recupero dei tickers.`);
    }

    if(!walletBalance.error) {
      let oldBalance = await getFromRedis('wallet_balance');
      let balanceMessage = newWalletMessage(walletBalance.currentBalance, oldBalance);
      message.push(balanceMessage)
      await saveToRedis('wallet_balance', walletBalance.currentBalance);
    } else {
      ctx.replyWithHTML(`‼️ Errore nel recupero del wallet.`);
    }

    bot.telegram.sendMessage(
      ChatID,
      message,
      {parse_mode: 'HTML'},
    );
  }, (tickerTimeFrame * 60 * 1000) ); // Set in minutes

}

module.exports = autoNotifications;