import bot from './bot-config';
import Markup from 'telegraf/markup';
import getTickers from './kraken-tikers';
import {saveToRedis, getFromRedis} from './redis';
import {newWalletMessage, newTickerMessage} from './messages';


// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// HEARS and REACTIONS

const hears = () => {
  // GET BALANCE WALLET
  // REPLY WITH WALLET BALANCE
  bot.hears('💰 Wallet', async (ctx) => {
    const walletBalance = await getWalletStatus();

    if(!walletBalance.error) {
      let oldBalance = await getFromRedis('wallet_balance');
      let balanceMessage = newWalletMessage(walletBalance.currentBalance, oldBalance);
      ctx.replyWithHTML(balanceMessage);
      await saveToRedis('wallet_balance', walletBalance.currentBalance);
    } else {
      ctx.replyWithHTML(`‼️ Errore nel recupero del wallet.`);
    }
  });

  // GET TICKERS
  // REPLY WITH TICKERS
  bot.hears('📊 Ticker', async (ctx) => {
    const tickers = await getTickers();
    // console.log('⚠️ TICKERS:', tickers);

    if(!tickers.error) {
      // Loop array
      for (let i in tickers) {

        //console.log('⚠️ SINGLE TICKER:', tickers[i]);
        let oldVPairalue = await getFromRedis(tickers[i][0]);
        let pairTickerMessage = newTickerMessage(tickers[i][1], oldVPairalue, tickers[i][2]);

        ctx.replyWithHTML(`${ pairTickerMessage }`,
        Markup.
          inlineKeyboard([
            Markup.callbackButton(`✅ Op. Pos. (0)`, 'OP_' + tickers[i][0]),
            Markup.callbackButton(`❌ Cl. Pos. (0)`, 'CP_' + tickers[i][0]),
          ]).extra());

        await saveToRedis(tickers[i][0], tickers[i][2]);
        // console.log('⚠️ testRedisSessionSave:', testRedisSessionSave);
      }
    } else {
      ctx.replyWithHTML(`‼️ Errore nel recupero dei tickers.`);
    }
  });

  // GET OPEN POSITIONS
  // REPLY WITH WALLET BALANCE
  bot.hears('🐃 Open Orders', async (ctx) => {
    const orders = await getOpenOrders();
    console.log('ORDERS:', orders);
    const arrOrders = _.toPairs(orders.list);

    if(orders.list.length > 0) {
      for(let i in arrOrders) {
        let order = arrOrders[i][1];
        // console.log('🐃 Positions Order: ', order);
        // console.log('🐃 Positions Order TXID: ', orders.taxIds[i]);
        // console.log('🐃 Positions Order REFID: ', order.refid); // Pass it to getOrdersInfo()
        let orderInfo = await getOrdersInfo(order.refid);
        // console.log('🐃 Original Order saved to Redis: ', orderInfo);

        let orderMessage = `<strong># ${order.refid} ${order.descr.pair}</strong> \nOrder: ${orderInfo.order} \nClose: ${orderInfo.close}`;
        ctx.replyWithHTML(orderMessage,
          Markup.
            inlineKeyboard([
              Markup.callbackButton(`Take profit`, 'take_profit'),
              Markup.callbackButton(`Close order`, 'close_order'),
            ]).extra());
      }
    } else {
      let orderMessage = `NESSUN ORDINE APERTO`;
      ctx.replyWithHTML(orderMessage);
    }
  });

  // GET SETTINGS
  // REPLY WITH SETTINGS STATUS
  bot.hears('⚙️ Settings', (ctx) => {
    let settingMessage = `⚙️ Settings: \nModalitá test: <strong>${RiskManagement.isTesting}</strong> \nTipo di ordine: <strong>${RiskManagement.orderType}</strong> \nCapitale per ogni trade: USD <strong>${RiskManagement.tradeCapitalRisk}</strong> \nTake profit a: <strong>+${RiskManagement.takeProfitPercentage}%</strong>`;
    ctx.replyWithHTML(settingMessage);
  });
}

module.exports = hears;