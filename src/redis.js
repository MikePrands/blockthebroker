// REDIS
import {promisify} from 'util';
import Redis from 'redis';
// Define Redis
const redis = Redis.createClient();

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

// SET REDIS SESSION FNs
const saveToRedis = async (key, value) => {
  let success = false;
  let redisSet = promisify(redis.set).bind(redis);

  // Save session state
  await redisSet(key, value)
    .then((res) => {
      // console.log('❇️ saveToRedis() redisSet key', key);
      // console.log('️❇️ saveToRedis() redisSet value', value);
      // console.log('❇️ saveToRedis() redisSet(key, value)', res);
      success = true;
    })
    .catch((err) => {
      console.log(' ❇️ ❌ saveToRedis() redisSession.saveSession(key, value)', err);
    });

  return success;
}

// GET REDIS SESSION
const getFromRedis = async (key) => {
  let value = false;
  let redisGet = promisify(redis.get).bind(redis);

  // Save session state
  await redisGet(key)
    .then((res) => {
      // console.log('♻️ getFromRedis() redisSession.getSession key', key);
      // console.log('♻️ getFromRedis() redisSession.getSession(key)', res);
      value = res;
    })
    .catch((err) => {
      console.log('️️♻️ ❌ getFromRedis() redisSession.getSession(key, value)', err);
    });

  return value;
}

module.exports = {
  saveToRedis: saveToRedis,
  getFromRedis: getFromRedis
}